package model

import (
	"time"
)

type User struct {
	ID string
	Firstname string
	Lastname string
	Username string
	Password string
	Email string
	LastLogin time.Time
}