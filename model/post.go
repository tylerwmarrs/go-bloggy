package model

import (
	"time"
)

type Post struct {
	ID string
	Author User
	Title string
	Slug string
	Tags []string
	Content string
	Published bool
	ModifyDate time.Time
}