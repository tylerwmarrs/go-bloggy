package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type AppConfig struct {
	// debug, release, test
	Mode string `json:"mode"`

	// logfile path to write to
	LogFile string `json:"logfile"`

	// path to templates
	Templates string `json:"templates"`

	// path to static
	Static string `json:"static"`

	Server struct {
		Host string `json:"host"`
		Port int    `json:"port"`
	} `json:"server"`

	DB struct {
		Mongo struct {
			DB             string   `json:"db"`
			ConnectionURIs []string `json:"connection_uris"`
		} `json:"mongo"`
	} `json:"db"`
}

func AppConfigFromFile(filePath string) (AppConfig, error) {
	config := AppConfig{}
	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		return AppConfig{}, err
	}

	err = json.Unmarshal(content, &config)
	if err != nil {
		return AppConfig{}, err
	}

	return config, nil
}

func (c *AppConfig) GetLogFile() (*os.File, error) {
	return os.Create(c.LogFile)
}
