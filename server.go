package main

import (
	"flag"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/contrib/renders/multitemplate"
	"go-bloggy/config"
	"go-bloggy/controller"
	"go-bloggy/db/mongo"
	"go-bloggy/repository"
	"go-bloggy/service"
	"os"
)

// Combine template files for rendering
func createMyRender(templatePath string) multitemplate.Render {
    r := multitemplate.New()
    r.AddFromFiles("index", templatePath + "layout.html", templatePath + "index.tmpl")
    r.AddFromFiles("newpost", templatePath + "layout.html", templatePath + "new_post.tmpl")
    r.AddFromFiles("categories", templatePath + "layout.html", templatePath + "categories.tmpl")
    r.AddFromFiles("rss", templatePath + "rss.html")
    r.AddFromFiles("search", templatePath + "layout.html", templatePath + "search.tmpl")
    r.AddFromFiles("login", templatePath + "layout.html", templatePath + "login.tmpl")

    return r
}

func main() {
	// Parse command line args
	configFlag := flag.String("config", "", "configuration file")
	flag.Parse()

	// set up config path
	configPath := "config.json"
	if *configFlag != "" {
		configPath = *configFlag
	}

	fmt.Println(fmt.Sprintf("Config Path: %s", configPath))

	// Read config file
	appConfig, err := config.AppConfigFromFile(configPath)
	if err != nil {
		panic(err)
		os.Exit(1)
	}

	// set gin in debug, release or test mode
	switch appConfig.Mode {
	case "release":
		gin.SetMode(gin.ReleaseMode)
	case "test":
		gin.SetMode(gin.TestMode)
	default:
		gin.SetMode(gin.DebugMode)
	}

	// Creates a gin router with default middleware:
	// logger and recovery (crash-free) middleware
	router := gin.Default()

	// Global middleware
	// logging
	if appConfig.LogFile != "" {
		logFile, err := appConfig.GetLogFile()
		if err != nil {
			panic(err)
			os.Exit(1)
		}

		defer logFile.Close()
		router.Use(gin.LoggerWithWriter(logFile))
	} else {
		router.Use(gin.Logger())
	}

	// recovery - never die on panic
	router.Use(gin.Recovery())

	// get db connection
	mongodb := mongo.NewMongo(appConfig.DB.Mongo.ConnectionURIs,
		appConfig.DB.Mongo.DB)

	// set up controllers, services etc
	pr := repository.Post{mongodb}
	ps := service.Post{pr}
	pc := controller.Post{ps}

	ur := repository.User{mongodb}
	us := service.User{ur}
	ac := controller.Authentication{us}

	// set up routes
	router.GET("/", pc.RecentPosts)
	router.GET("/newpost", pc.NewPost)
	router.GET("/search", pc.Search)
	router.GET("/categories", pc.Categories)
	router.GET("/rss", pc.Rss)
	router.GET("/login", ac.Login)
	router.GET("/logout", ac.Logout)

	// set up templates and static dirs
	router.HTMLRender = createMyRender(appConfig.Templates)
	router.Static("/static", appConfig.Static)

	// run the server
	router.Run(fmt.Sprintf("%s:%d", appConfig.Server.Host,
		appConfig.Server.Port))
}
