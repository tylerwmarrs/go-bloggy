package repository

import (
	_ "gopkg.in/mgo.v2/bson"
	"go-bloggy/db/mongo"
)

var postsCollectionName string = "posts"

type Post struct {
	Mongo mongo.Mongo
}