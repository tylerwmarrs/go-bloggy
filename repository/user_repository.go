package repository

import (
	_ "gopkg.in/mgo.v2/bson"
	"go-bloggy/db/mongo"
)

const usersCollectionName string = "users"

type User struct {
	Mongo mongo.Mongo
}