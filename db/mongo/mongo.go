package mongo

import (
	"gopkg.in/mgo.v2"
	"strings"
)

type Mongo struct {
	ConnectionURIs []string
	DB             string
	Session        *mgo.Session
}

func NewMongo(uris []string, db string) Mongo {
	m := Mongo{}
	m.ConnectionURIs = uris
	m.DB = db

	return m
}

func (m *Mongo) Connect() error {
	var err error
	m.Session, err = mgo.Dial(strings.Join(m.ConnectionURIs, ","))
	return err
}

func (m *Mongo) GetCollection(collection string) *mgo.Collection {
	return m.Session.DB(m.DB).C(collection)
}

func (m *Mongo) Close() {
	m.Session.Close()
}
