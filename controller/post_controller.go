package controller

import (
	"net/http"
	"github.com/gin-gonic/gin"
	"go-bloggy/service"
)

type Post struct {
	PostService service.Post
}

func (pc *Post) Search(c *gin.Context) {
	c.HTML(http.StatusOK, "search", gin.H{"title": "Search"})
}

func (pc *Post) Categories(c *gin.Context) {
	c.HTML(http.StatusOK, "categories", gin.H{"title": "Categories"})
}

func (pc *Post) Rss(c *gin.Context) {
	c.HTML(http.StatusOK, "rss", gin.H{"title": "RSS"})
}

func (pc *Post) RecentPosts(c *gin.Context) {
	c.HTML(http.StatusOK, "index", gin.H{"title": "Home"})
}

func (pc *Post) NewPost(c *gin.Context) {
	c.HTML(http.StatusOK, "newpost", gin.H{"title": "New Post"})
}
