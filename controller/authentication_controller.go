package controller

import (
	"net/http"
	"github.com/gin-gonic/gin"
	"go-bloggy/service"
)

type Authentication struct {
	UserService service.User
}

func (ac *Authentication) Login(c *gin.Context) {
	c.HTML(http.StatusOK, "login", gin.H{"title": "Login"})
}

func (ac *Authentication) Logout(c *gin.Context) {
	//TODO
}